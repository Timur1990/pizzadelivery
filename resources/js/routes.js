import Vue from 'vue';
import VueRouter from 'vue-router';

import MainPage from './components/MainPageComponent.vue';
import LoginPage from './components/auth/LoginComponent.vue';
import RegisterPage from './components/auth/RegisterComponent.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    // mode: 'history',
    routes:[
        {
            path:'/',
            component:MainPage
        },
        {
            path:'/auth/login',
            component:LoginPage
        },
        {
            path:'/auth/register',
            component:RegisterPage
        },
    ]
});

export default router